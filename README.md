Role Name
=========

This installs the small utilities used in the EXA application installation. Components like Nginx, Redis, Node, and PostgreSQL will have their own role that can install their needed versions.

Requirements
------------

Windows OS with WinRM and OpenSSH configured.

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - exa-prereqs

License
-------

Konica Minolta Healthcare Amarica

Author Information
------------------

Chris Fouts, Sr DevOps Engineer
